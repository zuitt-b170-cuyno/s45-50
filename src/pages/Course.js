import React from 'react'
import CourseCard from './../components/CourseCard'

import Container from 'react-bootstrap/Container'
import courses from './../mock-data/courses'

function Course() {
  const CourseCards = courses.map(course => <CourseCard course={course} />)

	return (
		<Container fluid>
			{ CourseCards }
		</Container>
	)
}

export default Course