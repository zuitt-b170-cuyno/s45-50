import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunitues for Everyone, Everywhere</p>
					<Button variant='primary'>Enroll Now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}

export default Banner