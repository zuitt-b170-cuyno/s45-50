import React, { useState, useEffect } from 'react'
import Container from 'react-bootstrap/Container'

function Counter() {
    const [ count, setCount ] = useState(0)
    useEffect(() => { document.title = `You clicked ${count} times` }, [count])

    return (
        <Container fluid>
            <p>You clicked {count} times</p>
            <p id='number'></p>
            <button onClick={ () => setCount(count + 1) }>Click Me!</button>
        </Container>
    )
}

export default Counter