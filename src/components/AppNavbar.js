import React, { Fragment, useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import UserContext from '../userContext'

function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext)
  const navigate = useNavigate()

  const logout = () => {
    unsetUser()
    navigate('/login')
  }

  const rightNav = !user.access ? (
    <Fragment>
      <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
      <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
    </Fragment>
  ) : (
    <Fragment>
      <Nav.Link onClick={logout}>Logout</Nav.Link>
    </Fragment>
  )

  return (
    <Navbar bg='light' expand='lg'>
      <Navbar.Brand as={Link} to='/'>Zuitt Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='mr-auto'>
          <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
          <Nav.Link as={NavLink} to='/courses'>Course</Nav.Link>
        </Nav>
        <Nav className='ml-auto'>{rightNav}</Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default AppNavbar