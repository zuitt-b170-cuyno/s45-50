import React, { useState, useEffect } from 'react'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

function CourseCard(props) {
	const course = props.course

	const [ count, setCount ] = useState(0)
	const [ seat, setSeat ] = useState(30)
	const [ isDisabled, setIsDisabled ] = useState(false)

	const enroll = () => { 
		if (seat <= 0) 
			alert('No more seats')
		else {
			setCount(count + 1) 
			setSeat(seat - 1)
		}
	}

	useEffect(() => {
		if (seat === 0)
			setIsDisabled(true)
	}, [seat])

  return (
		<Row className='my-3'>
			<Col>
				<Card>
					<Card.Body>
						<Card.Title className='font-weight-bold'>{course.name}</Card.Title>
						<Card.Subtitle className='font-weight-bold'>Description</Card.Subtitle>
						<Card.Text>{course.description}</Card.Text>
						<Card.Subtitle className='font-weight-bold'>Price</Card.Subtitle>
						<Card.Text>PHP {course.price}</Card.Text>
						<Card.Subtitle className='font-weight-bold'>Enrollees</Card.Subtitle>
						<Card.Text>{ seat } remaining</Card.Text>
						<Button disabled={isDisabled} variant='primary' onClick={enroll}>Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

export default CourseCard