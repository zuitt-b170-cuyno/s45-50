const courses = [
    {
        id: 'wdc001',
        name: 'PHP - Laravel',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum porttitor sapien, quis fringilla nibh placerat at. Aliquam laoreet viverra aliquam. Quisque vestibulum augue posuere diam pulvinar pellentesque. Nulla facilisi. Cras tincidunt nisl ac erat volutpat dictum. Donec nunc purus, efficitur et semper non, venenatis et nulla. Nam tellus tellus, ultrices vitae sapien nec, accumsan pellentesque massa. Pellentesque vel neque non ante blandit sollicitudin eu sit amet neque. Cras placerat, felis at rhoncus maximus, neque quam efficitur odio, a accumsan elit tortor at lorem. Ut vitae tincidunt dui. ',
        price: 50000,
        onOffer: true
    },
    {
        id: 'wdc002',
        name: 'Python - Django',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum porttitor sapien, quis fringilla nibh placerat at. Aliquam laoreet viverra aliquam. Quisque vestibulum augue posuere diam pulvinar pellentesque. Nulla facilisi. Cras tincidunt nisl ac erat volutpat dictum. Donec nunc purus, efficitur et semper non, venenatis et nulla. Nam tellus tellus, ultrices vitae sapien nec, accumsan pellentesque massa. Pellentesque vel neque non ante blandit sollicitudin eu sit amet neque. Cras placerat, felis at rhoncus maximus, neque quam efficitur odio, a accumsan elit tortor at lorem. Ut vitae tincidunt dui. ',
        price: 45000,
        onOffer: true
    },
    {
        id: 'wdc003',
        name: 'PHP - Spring Boot',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum porttitor sapien, quis fringilla nibh placerat at. Aliquam laoreet viverra aliquam. Quisque vestibulum augue posuere diam pulvinar pellentesque. Nulla facilisi. Cras tincidunt nisl ac erat volutpat dictum. Donec nunc purus, efficitur et semper non, venenatis et nulla. Nam tellus tellus, ultrices vitae sapien nec, accumsan pellentesque massa. Pellentesque vel neque non ante blandit sollicitudin eu sit amet neque. Cras placerat, felis at rhoncus maximus, neque quam efficitur odio, a accumsan elit tortor at lorem. Ut vitae tincidunt dui. ',
        price: 60000,
        onOffer: true
    }
]

export default courses