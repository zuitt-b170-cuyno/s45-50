import React, { useState, useEffect, useContext } from 'react'
import { Form, Container, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from './../userContext'
import { Navigate } from 'react-router-dom'

function Login() {
	const { user, setUser } = useContext(UserContext)
	const [ email, setEmail ] = useState("")
	const [ password, setPassword ] = useState("")
	const [ isDisabled, setIsDisabled ] = useState(true)

	const login = event => {
		event.preventDefault()
		fetch('http://localhost:4000/api/users/login', {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(response => {
			if (response.access) {
				localStorage.setItem('access', response.access)
				setUser({ access: response.access })
			}
			else {
				Swal.fire('Login Unsuccessful', '', 'error')
				setEmail("")
				setPassword("")
			}
		})
	}

	useEffect(() => {
		email && password ? setIsDisabled(false) : setIsDisabled(true)
	}, [email, password])

	if (user.access) 
		return <Navigate replace to='/' />
		
	return (
		<Container>
			<h1 className='mt-5 font-weight-bold'>Login</h1>
			<Form onSubmit={login}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type='email' placeholder='Enter email address' required value={email} onChange={ event => setEmail(event.target.value) } />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type='password' placeholder='Enter password' required value={password} onChange={ event => setPassword(event.target.value) } />
				</Form.Group>
				<Button variant='success' type='submit' disabled={isDisabled}>Login</Button>
			</Form>
		</Container>
	)
}

export default Login