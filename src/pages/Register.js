import React, { useState, useEffect, useContext } from 'react'
import { Form, Container, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from './../userContext'
import { useNavigate, Navigate } from 'react-router-dom'

function Register() {
	const { user } = useContext(UserContext)
	const navigate = useNavigate()
	const [ firstName, setFirstName ] = useState("")
	const [ lastName, setLastName ] = useState("")
	const [ mobileNumber, setMobileNumber ] = useState("")
	const [ email, setEmail ] = useState("")
	const [ password, setPassword ] = useState("")
	const [ passwordConfirm, setPasswordConfirm ] = useState("")
	const [ isDisabled, setIsDisabled ] = useState(true)

	useEffect(() => {
		const isComplete = firstName && lastName && mobileNumber && email && password && password && passwordConfirm
		isComplete ? setIsDisabled(false) : setIsDisabled(true)
	}, [firstName, lastName, mobileNumber, email, password, passwordConfirm])

	if (user.access) 
		return <Navigate replace to='/courses' />

	const register = event => {
		event.preventDefault()
		if (password !== passwordConfirm) 
			Swal.fire('Registration Failed', 'Password does not match', 'error')
		else if (mobileNumber.length < 11)
			Swal.fire('Registration Failed', 'Mobile number should be at least 11 digits', 'error')
		else {
			fetch('http://localhost:4000/api/users/checkEmail', { 
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({ email: email })
			})
			.then(response => response.json())
			.then(response => {
				if (response)
					Swal.fire('Duplicate Email Found', 'Please provide a different email', 'error')
				else {
					fetch('http://localhost:4000/api/users/register', {
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNumber: mobileNumber,
							email: email,
							password: password
						})
					})
					.then(() => {
						Swal.fire('Registered successfully', 'You may now log in.', 'success')
						setFirstName("")
						setLastName("")
						setMobileNumber("")
						setEmail("")
						setPassword("")
						setPasswordConfirm("")
					})
					.then(() => navigate('/login'))
				}
			})
		}		
	}
	
	return (
		<Container>
			<Form onSubmit={register}>
				<Form.Group className='mt-2'>
					<Form.Label>First Name</Form.Label>
					<Form.Control type='text' placeholder='Enter first name' required value={firstName} onChange={ event => setFirstName(event.target.value) } />
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type='text' placeholder='Enter last name' required value={lastName} onChange={ event => setLastName(event.target.value) } />
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type='text' placeholder='Enter mobile number' required value={mobileNumber} onChange={ event => setMobileNumber(event.target.value) } />
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type='email' placeholder='Enter email address' required value={email} onChange={ event => setEmail(event.target.value) } />
					<Form.Text className='text-muted'>
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type='password' placeholder='Enter password' required value={password} onChange={ event => setPassword(event.target.value) } />
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type='password' placeholder='Confirm password' required value={passwordConfirm} onChange={ event => setPasswordConfirm(event.target.value) } />
					<Form.Text className='text-danger' id='error-message'></Form.Text>
				</Form.Group>
				<Button variant='primary' type='submit' disabled={isDisabled}>Register</Button>
			</Form>
		</Container>
	)
}

export default Register