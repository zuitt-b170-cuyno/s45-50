import React from 'react'
import Banner from './../components/Banner'
import Highlight from './../components/Highlight'
import Container from 'react-bootstrap/Container'

function Home() {
  return (
		<Container fluid>
      <Banner /> 
      <Highlight />
    </Container>
	)
}

export default Home