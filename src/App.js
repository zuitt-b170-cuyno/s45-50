import React, { useState } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'

import AppNavbar from './components/AppNavbar'
import Login from './pages/Login'
import Register from './pages/Register'
import Course from './pages/Course'
import Home from './pages/Home'
import PageNotFound from './pages/PageNotFound'
import UserContext from './userContext'

function App() {
	const [ user, setUser ] = useState({ access: localStorage.getItem('access') })

	const unsetUser = () => {
		localStorage.clear()
		setUser({ access: null })
	}

	return (
		<UserContext.Provider value={{user, setUser, unsetUser}}>
			<Router>
				<AppNavbar />
				<Routes>
					<Route path='/' element={<Home />} />
					<Route path='/courses' element={<Course />} />
					<Route path='/register' element={<Register />} />
					<Route path='/login' element={<Login />} />
					<Route path='*' element={<PageNotFound />} />
				</Routes>
			</Router>
		</UserContext.Provider>
	)
}

export default App