import React from 'react'
import { Container, Link } from 'react-bootstrap'

function PageNotFound() {
  return (
		<Container fluid>
			<h2 className='font-weight-bold mt-2'>Page Not Found</h2>
			<span>Go back to <a href='/'>Homepage</a></span>
		</Container>
	)
}

export default PageNotFound